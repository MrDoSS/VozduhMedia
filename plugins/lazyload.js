import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/wait.jpg',
  loading: '/wait.jpg',
  attempt: 3
})
