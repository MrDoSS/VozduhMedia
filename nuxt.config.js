module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'vozduh-vue',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A vozduh.media site' },
      { name: 'apple-mobile-web-app-title', content: 'Vozduh.media' },
      { name: 'application-name', content: 'Vozduh.media' },
      { name: 'msapplication-TileColor', content: '#00b7f4' },
      { name: 'theme-color', content: '#00b7f4' },
      { property: 'og:title', content: 'Digital агенство Vozduh.media' },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'https://vozduh.media/' },
      { property: 'og:site_name', content: 'Vozduh.media' },
      { property: 'og:description', content: 'A vozduh.media site' },
      { property: 'og:image', content: '/vozduhmedia.png' },
    ],
    link: [
      { rel: 'apple-touch-icon', sizes: '57x57', href: '/apple-touch-icon.png' },
      { rel: 'icon', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon',  href: '/safari-pinned-tab.svg', color: '#00b7f4' },
    ]
  },
  css: ['~assets/css/style.css'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#ffffff', height: '5px' },

  plugins: ['~plugins/scrollto', '~plugins/lazyload', "~plugins/map"],
  modules: [
    '@nuxtjs/axios',
    'bootstrap-vue/nuxt',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: 48981425,
        webvisor: true,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        trackHash:true
      }
    ],

  ],
  axios: {
    https: true,
    proxyHeaders: false
  },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
