import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC60oJ1noN0ltKGOFg324RtJrkj9BxnGeA',
  }
})
