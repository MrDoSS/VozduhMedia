import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    projects: [],
    cases: [],
  },
  mutations: {
    setProjects(state, projects) {
      state.projects = projects
    },
    setCases(state, cases) {
      state.cases = cases
    },
  },
  actions: {
    async nuxtServerInit({commit}, app) {
      let projects = await app.$axios.get('https://api.vozduh.media/projects/')
      let cases = await app.$axios.get('https://api.vozduh.media/cases/')
      commit('setProjects', projects.data)
      commit('setCases', cases.data)
    },
  }
})

export default store
